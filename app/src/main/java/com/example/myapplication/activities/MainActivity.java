package com.example.myapplication.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.WellcomeActivity;
import com.example.myapplication.activities.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;


public class MainActivity extends AppCompatActivity {
    FirebaseAuth auth;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progress = findViewById(R.id.progressbar);
        progress.setVisibility(View.GONE);
        auth = FirebaseAuth.getInstance();

        if(auth.getCurrentUser() != null) {
            progress.setVisibility(View.VISIBLE);
            startActivity(new Intent(this, WellcomeActivity.class));
            Toast.makeText(this, "redirect to home page", Toast.LENGTH_SHORT).show();
            finish();
        }
    };

    public void login(View view ) {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void register(View view ) {
        startActivity(new Intent(this, LoginActivity.class));
    }

}