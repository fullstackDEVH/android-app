package com.example.myapplication.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;

import com.example.myapplication.WellcomeActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity {
    Button btnSignIn;
    EditText email, password;
    TextView signUp;

    FirebaseDatabase database;
    FirebaseAuth auth;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        auth = FirebaseAuth.getInstance();

        email = findViewById(R.id.login_txt_email);
        password = findViewById(R.id.login_txt_pass);
        btnSignIn = findViewById(R.id.login_btn);
        signUp = findViewById(R.id.login_tv2);
        progress = findViewById(R.id.progressbar);
        progress.setVisibility(View.GONE);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUser();
                progress.setVisibility(View.VISIBLE);
            }
        });
    }

    public void switchSignUpActivity(View v) {
        Intent i = new Intent(this, RegisterActivity.class);
        startActivity(i);
    }

    private void loginUser() {
        String userEmail = email.getText().toString();
        String userPassword = password.getText().toString();

        if(TextUtils.isEmpty(userEmail)) {
            Toast.makeText(this, "email is empty !!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(userPassword)) {
            Toast.makeText(this, "password is empty !!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(userPassword.length() < 6 ) {
            Toast.makeText(this, "password length must be greater than 6 character!!", Toast.LENGTH_SHORT).show();
            return;
        }

        auth.signInWithEmailAndPassword(userEmail, userPassword)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            progress.setVisibility(View.GONE);

                            Toast.makeText(LoginActivity.this, "Login successfully !!", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(LoginActivity.this, WellcomeActivity.class));
                        }else {
                            Toast.makeText(LoginActivity.this, "Error: " + task.getException(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}