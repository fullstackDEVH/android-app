package com.example.myapplication.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.activities.LoginActivity;
import com.example.myapplication.models.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {
    Button btnSignUp;
    EditText name, email, password;
    TextView signIn;

    FirebaseDatabase database;
    FirebaseAuth auth;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btnSignUp = findViewById(R.id.regis_btn);
        name = findViewById(R.id.reg_txt_name);
        email = findViewById(R.id.reg_txt_email);
        password = findViewById(R.id.reg_txt_pass);
        progress = findViewById(R.id.progressbar);
        progress.setVisibility(View.GONE);

        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createUser();
                progress.setVisibility(View.VISIBLE);
            }
        });
    }

    public void switchSignInActivity(View v) {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }

    private void createUser() {
        String userName = name.getText().toString();
        String userEmail = email.getText().toString();
        String userPassword = password.getText().toString();

        if(TextUtils.isEmpty(userName)) {
            Toast.makeText(this, "name is empty !!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(userEmail)) {
            Toast.makeText(this, "email is empty !!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(userPassword)) {
            Toast.makeText(this, "password is empty !!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(userPassword.length() < 6 ) {
            Toast.makeText(this, "password length must be greater than 6 character!!", Toast.LENGTH_SHORT).show();
            return;
        }
        // Create user
       auth.createUserWithEmailAndPassword(userEmail, userPassword)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            UserModel userModel = new UserModel(userName,userPassword,userEmail);
                            String id = task.getResult().getUser().getUid();

                            database.getReference().child("User").child(id).setValue(userModel);
                            progress.setVisibility(View.GONE);

                            Toast.makeText(RegisterActivity.this, "Create Successfully !!!"+id, Toast.LENGTH_SHORT).show();
                        }else {
                            progress.setVisibility(View.GONE);
                            Toast.makeText(RegisterActivity.this, "Error: " + task.getException(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}